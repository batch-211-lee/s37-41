/*
	
App: Booking System API

Scenario:
	A course bookin system application where users can enroll into a course

Type: Course Booking System (Web App)

Description:
	A course booking system application where users can enrolls into a course
	Allows an admin to do CRUD operations
	Allows users to register into our database

Features:
	-User Login/User Authentication
	-User Registration

	Customer/Authenticated Users:
	-View active courses (all active courses)
	-Enroll Courses

	Admin Users:
	-Add course
	-Update course
	-Archive/Unarchive a course (soft delete/reactivate the course)
	-View Course (All courses active/inactive)
	-View or manage user accounts**

	All users (Guest,customers, admin)
	-View active courses
*/

//Data Model for the Booking System

//Two-way embedding

/*

user{
	id - unique identifier for our document
	firstName,
	lastName,
	email,
	password,
	mobileNumber,
	isAdmin,
	enrollments:[
		{
			id - document identifier,
			courseId - the unique identifier,
			courseName,
			isPaid,
			dateEnrolled
		}
	]
}

course{
	id
	name,
	description,
	price,
	slots,
	isActive,
	createdOn,
	enrolles: [
		{
			id
			userId - unique identifier for the user,
			isPaid,
			dateEnrolled
		}
	]
}
*/

//With Referencing

/*
	user{
		id - unique identifier for our document
		firstName,
		lastName,
		email,
		password,
		mobileNumber,
		isAdmin,
	}

	course{
		id
		name,
		description,
		price,
		slots,
		isActive,
		createdOn,
	}

	enrollment {
			id - document identifier,
			userID - unique identifier for the user,
			courseId - the unique identifier,
			courseName,
			isPaid,
			dateEnrolled
	}
*/