const Course = require("../models/Course");
const User = require("../models/User");
//Create a new course
/*
	steps:
	1. create a new Course object using the mongoose model and the information from the reqBody and the id from the header 
*/


// module.exports.addCourse = (data) => {	
// 	if (data.isAdmin) {
// 		let newCourse = new Course({
// 			name : data.course.name,
// 			description : data.course.description,
// 			price : data.course.price
// 		});
// 		return newCourse.save().then((course, error) => {
// 			if (error) {
// 				return false;
// 			} else {
// 				return true;
// 			};
// 		});
// 	} else {
// 		return false;
// 	};
// };

//Solution 2

module.exports.addCourse = (data) => {

	console.log(data)

	if (data.isAdmin) {
		let newCourse = new Course({
			name : data.course.name,
			description : data.course.description,
			price : data.course.price
		});

		console.log(newCourse);

		return newCourse.save().then((course, error) => {
			if (error) {
				return false;
			} else {
				return true;
			};
		});
	} else {
		return false;
	};
};

//Retrieve all courses

/*
	Retrieve all the courses from the database
		Model.find({})
*/

module.exports.getAllCourses = () =>{
	return Course.find({}).then(result=>{
		return result;
	});
};

//Retrieve all active courses
/*
	1. Retrieve all the course from the database with the propert of "isActive" to true
*/

module.exports.getAllActive = () =>{
	return Course.find({isActive:true}).then(result=>{
		return result;
	});
};

//Retrieving a specific course
/*
	1. retrieve the course that matches the course ID provided from the URL
*/

module.exports.getCourse = (reqParams)=>{
	return Course.findById(reqParams.courseId).then(result=>{
		return result;
	});
};

//Updating course
/*
	1. Create a vairable "updateCourse" which will contain the information retrieved from the request body
	2. Find and update the course using the course ID from the request params property and variable "updatedCourse" containing the information from the request body
*/

module.exports.updateCourse = (reqParams,reqBody)=>{
	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	};
	return Course.findByIdAndUpdate(reqParams.courseId,updatedCourse).then((course,error)=>{
		if(error){
			return false;
		}else {
			return true;
		};
	});
};

//Archiving course
module.exports.archiveCourse = (reqParams) =>{
	let archivedCourse = {
		isActive: false
	};
	return Course.findByIdAndUpdate(reqParams.courseId,archivedCourse).then((course,error)=>{
		if(error){
			return false;
		}else{
			return course;
		};
	});
};

//Activating course
module.exports.activateCourse = (reqParams) =>{
	return Course.findByIdAndUpdate(reqParams,{isActive: true}).then((course,error)=>{
		if(error){
			return false;
		}else{
			return true;
		};
	});
};